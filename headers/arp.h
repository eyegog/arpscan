#ifndef ARP_H
#define ARP_H

#include<stdint.h>

#define ARP_HEADER_LEN sizeof(struct arp_hdr)

#define ARP_OPER_REQUEST        1
#define ARP_OPER_REPLY          2

/* ARP_OPER in network byte order */
#define arp_operation(arphdr) (((struct arp_hdr *)(arphdr))->op)
/* ARP sha */
#define arp_sha(arphdr) (((struct arp_hdr *)(arphdr))->sha)
/* ARP tha */
#define arp_tha(arphdr) (((struct arp_hdr *)(arphdr))->tha)
/* ARP spa */
#define arp_spa(arphdr) (((struct arp_hdr *)(arphdr))->spa)
/* ARP tpa */
#define arp_tpa(arphdr) (((struct arp_hdr *)(arphdr))->tpa)

struct arp_hdr {
    uint16_t htype;
    uint16_t ptype;
    uint8_t hlen;
    uint8_t plen;
    uint16_t op;
    uint8_t sha[6];
    uint8_t spa[4];
    uint8_t tha[6];
    uint8_t tpa[4];
} __attribute__((packed));

void packageARP(uint8_t *buffer, uint16_t op, uint8_t *sha,
           uint8_t *spa, uint8_t *tha, uint8_t *tpa);
void parse_arp(const unsigned char *header_start);

#endif
